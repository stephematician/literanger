# Purpose of this pipeline is to check the (built) package on Linux and
# Windows (Beta). See <gitlab.com/stephematician/r-gitlab-ci>.

stages:
  - check
  - test

image: node:20

variables:
  R_KEEP_PKG_SOURCE: "yes"
  _R_CHECK_CRAN_INCOMING_: "false"
  _R_CHECK_FORCE_SUGGESTS_: "true"

default:
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - .gitlab/cache

include:
  - component: $CI_SERVER_FQDN/stephematician/r-gitlab-ci/gitlab_ci_utility@0.0.2
  - component: $CI_SERVER_FQDN/stephematician/r-gitlab-ci/setup_pandoc@0.0.2
  - component: $CI_SERVER_FQDN/stephematician/r-gitlab-ci/setup_r@0.0.2
  - component: $CI_SERVER_FQDN/stephematician/r-gitlab-ci/setup_r_deps@0.0.2
    inputs:
      extra-packages: >-
        any::rcmdcheck
        any::covr any::DT any::htmltools
  - component: $CI_SERVER_FQDN/stephematician/r-gitlab-ci/check_r_package@0.0.2


#############################################
####    Platforms to check package on    ####
#############################################

.parallel-job-bash: &parallel-job
  parallel:
    matrix:
      - &linux-matrix
        PLATFORM: linux
        MACHINE_SIZE: small
        ARCH: amd64
        # Alternative - only perform check on current release
        # R_TYPE: release
        R_TYPE: [ devel, release, oldrel-1 ]
  before_script:
    - !reference [ .gitlab_ci_utility-job-bash, before_script ]
  tags: &linux-tags
    - saas-${PLATFORM}-${MACHINE_SIZE}-${ARCH}
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

.parallel-job-pwsh:
  << : *parallel-job
  parallel:
    matrix:
      - PLATFORM: windows
        MACHINE_SIZE: medium
        ARCH: amd64
        R_TYPE: release
  before_script:
    # FIXME: standard checkout procedure does not add symlink on Windows
    - git config core.symlinks true
    - git checkout -- src/literanger
    - !reference [ .gitlab_ci_utility-job-pwsh, before_script ]


###############################################################
####    Job(s) for checking package on Linux and Windows   ####
###############################################################

check_r_package-job-bash:
  extends: .parallel-job-bash
  stage: check
  before_script:
    - !reference [ .parallel-job-bash, before_script ]
    - !reference [ .setup_r-job-bash, before_script ]
    - !reference [ .setup_r_deps-job-bash, before_script ]
    - !reference [ .check_r_package-job-bash, before_script ]
  script:
    - !reference [ .setup_r-job-bash, script ]
    - !reference [ .setup_r_deps-job-bash, script ]
    - !reference [ .check_r_package-job-bash, script ]
  artifacts:
    !reference [ .check_r_package-job-bash, artifacts ]

check_r_package-job-pwsh:
  extends: .parallel-job-pwsh
  stage: check
  before_script:
    - !reference [ .parallel-job-pwsh, before_script ]
    - !reference [ .setup_pandoc-job-pwsh, before_script ]
    - !reference [ .setup_r-job-pwsh, before_script ]
    - !reference [ .setup_r_deps-job-pwsh, before_script ]
    - !reference [ .check_r_package-job-pwsh, before_script ]
  script:
    - !reference [ .setup_pandoc-job-pwsh, script ]
    - !reference [ .setup_r-job-pwsh, script ]
    - !reference [ .setup_r_deps-job-pwsh, script ]
    - !reference [ .check_r_package-job-pwsh, script ]
  artifacts:
    !reference [ .check_r_package-job-pwsh, artifacts ]


###########################################
####    Job to assess code coverage    ####
###########################################

coverage-job-bash:
  stage: test
  when: on_success
  allow_failure: true
  rules:
    - if: '($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH) && ($CI_PIPELINE_SOURCE != "merge_request_event")'
  variables:
    << : *linux-matrix
    R_TYPE: release
  tags: *linux-tags
  before_script:
    - !reference [ .gitlab_ci_utility-job-bash, before_script ]
    - !reference [ .setup_r-job-bash, before_script ]
    - !reference [ .setup_r_deps-job-bash, before_script ]
  script:
    - !reference [ .setup_r-job-bash, script ]
    - !reference [ .setup_r_deps-job-bash, script ]
    - >-
      Rscript -e 'covr::gitlab(quiet = FALSE, file="covr/coverage.html")'
    - mv covr ./.gitlab
  coverage: '/Coverage: \d+\.\d+/'
  artifacts:
    paths:
      - .gitlab/covr

