# literanger submission

Submit version 0.1.1: performance enhancement

__clang-UBSAN gcc-UBSAN__: Issues reported in 0.1.0 were caused by Rcereal using
an outdated version of [cereal](https://uscilab.github.io/cereal/). Rcereal has
been updated to use 1.3.2; gcc-UBSAN no longer reports errors.

`NOTE`s about library size are caused by linking to cpp11, Rcereal, and standard
libraries on Linux/OS-X.


## win-builder `R CMD check` result summary

    * DONE
    Status: OK


## mac-builder `R CMD check` note

    * checking installed package size ... NOTE
      installed size is 10.4Mb
      sub-directories of 1Mb or more:
        libs  10.3Mb
    Status: 1 NOTE


## GitLab CI/CD `R CMD check` results summary

### Linux - docker image: node:20

R versions 4.3.3, 4.4.1, and 4.5.0.

    > checking installed package size ... NOTE
        installed size is 14.6Mb
        sub-directories of 1Mb or more:
          libs  14.4Mb
    Status: 1 NOTE

